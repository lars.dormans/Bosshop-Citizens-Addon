package me.creepercow.shopaddon.Commands;

import me.creepercow.shopaddon.Main;
import me.creepercow.shopaddon.Managers.DataManager;
import me.creepercow.shopaddon.Tasks.PlayerTimeOutTask;
import me.creepercow.shopaddon.Tasks.TimeOutTask;
import net.citizensnpcs.api.npc.NPC;
import org.black_ixx.bossshop.api.BossShopAPI;
import org.black_ixx.bossshop.core.BSShop;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class BindCommand implements CommandExecutor{
    public static HashMap<Player, BSShop> requestMap = new HashMap<>();
    public static HashMap<Player,UUID> playerShopMap = new HashMap<>();
    public static List<Player> removeRequest = new ArrayList<>();
    BossShopAPI bossShop;
    Main instance;
    public BindCommand(BossShopAPI bossShop, Main instance) {
        this.bossShop = bossShop; this.instance = instance;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if(!command.getName().equalsIgnoreCase("npcshop")){
            return false;
        }
        if(args.length == 0){
            help(commandSender);
            return false;
        }
        if(args[0].equalsIgnoreCase("add")){
            if(!(commandSender instanceof Player)){
                commandSender.sendMessage("Can only be exeucted by players");
                return false;
            }
            Player p = (Player) commandSender;
            if(args.length<2){
                commandSender.sendMessage(ChatColor.RED+"Missing shop name");
                return false;
            }
            BSShop shop = null;
            try {
                shop = bossShop.getShop(args[1]);
            }catch (NullPointerException e){
                commandSender.sendMessage(ChatColor.RED+"Invalid shop name");
                return false;
            }
            if(shop == null){
                commandSender.sendMessage(ChatColor.RED+"Invalid shop name");
                return true;
            }
            requestMap.put(p, shop);
            Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new TimeOutTask(p), 10*20);
            p.sendMessage(ChatColor.GOLD+"Click the NPC you wanna bind to");
            return true;
        }
        if(args[0].equalsIgnoreCase("remove")){
            if(!(commandSender instanceof Player)){
                commandSender.sendMessage("Can only be executed by players");
                return false;
            }
            Player p = (Player) commandSender;
            removeRequest.add(p);
            Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new TimeOutTask(p), 10*20);
            p.sendMessage(ChatColor.GOLD+"Click the NPC you wanna remove");
            return true;
        }
        return false;
    }
    public void help(CommandSender sender){
        sender.sendMessage(ChatColor.YELLOW+"Following commands can be used\n"
        + "/npcshop add <shopname>\n"
        + "/npcshop remove\n"
        + "/npcshop addplayer <playername>");
    }
    public static void CommandContinue(NPC npc, Player p, BSShop shop){
        DataManager.addNPC(npc, shop);
        p.sendMessage(ChatColor.GREEN+"Bound successful");
        requestMap.remove(p);
    }
    public static void PlayerCommandContinue(NPC npc,Player p, UUID uuid){
        DataManager.addPlayerNPC(npc,uuid);
        p.sendMessage(ChatColor.GREEN+"Bound successful");
        playerShopMap.remove(p);
    }
    public static void RemoveContinue(NPC npc, Player p){
        if(!DataManager.isShopNPC(npc)){
            p.sendMessage(ChatColor.RED+"This is not a shop npc");
            removeRequest.remove(p);
            return;
        }
        DataManager.removeNPC(npc);
        removeRequest.remove(p);
        p.sendMessage(ChatColor.GREEN+"NPC removed");
    }
}
