package me.creepercow.shopaddon.Tasks;

import me.creepercow.shopaddon.Commands.BindCommand;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class TimeOutTask implements Runnable{
    Player p;
    public TimeOutTask(Player p) {
        this.p = p;
    }

    @Override
    public void run() {
        if(!(BindCommand.requestMap.containsKey(p))&&!(BindCommand.removeRequest.contains(p))){
            return;
        }
        BindCommand.removeRequest.remove(p);
        BindCommand.requestMap.remove(p);
        p.sendMessage(ChatColor.RED+"Command timed out rerun the command please");
    }
}
