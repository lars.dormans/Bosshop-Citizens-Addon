package me.creepercow.shopaddon;

import me.creepercow.shopaddon.Commands.BindCommand;
import me.creepercow.shopaddon.Managers.DataManager;
import me.creepercow.shopaddon.Managers.RightClickNPC;
import org.black_ixx.bossshop.addon.playershops.PlayerShops;
import org.black_ixx.bossshop.api.BossShopAddon;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

public class Main extends BossShopAddon {
    PlayerShops playerShops;
    @Override
    public String getAddonName() {
        return "CitizensIntergration";
    }

    @Override
    public String getRequiredBossShopVersion() {
        return "1.8.0";
    }

    @Override
    public void enableAddon() {
        this.getServer().getPluginManager().registerEvents(new RightClickNPC(), this);
        new DataManager(this, this.getBossShop().getAPI());
        getCommand("npcshop").setExecutor(new BindCommand(this.getBossShop().getAPI(), this));
    }

    @Override
    public void disableAddon() {

    }

    @Override
    public void bossShopReloaded(CommandSender commandSender) {

    }

    public PlayerShops getPlayerShops() {
        return playerShops;
    }
}
