package me.creepercow.shopaddon.Managers;

import me.creepercow.shopaddon.Main;
import net.citizensnpcs.api.npc.NPC;
import org.black_ixx.bossshop.api.BossShopAPI;
import org.black_ixx.bossshop.core.BSShop;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class DataManager {
    static File dataf;
    static FileConfiguration data;
    static Main plugin;
    static BossShopAPI bossShop;
    @SuppressWarnings("AccessStaticViaInstance")
    public DataManager(Main plugin, BossShopAPI bossShop) {
        this.plugin = plugin;
        this.bossShop = bossShop;
        createFiles();
    }
    private void createFiles(){
        dataf = new File(plugin.getDataFolder(), "data.yml");

        if(!dataf.exists()){
            dataf.getParentFile().mkdirs();
            plugin.saveResource("data.yml", false);
        }
        try{
            if (!plugin.getDataFolder().exists()){
                plugin.getDataFolder().mkdirs();
            }
            data = new YamlConfiguration();
            data.load(dataf);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void saveData(){
        try {
            data.save(dataf);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static FileConfiguration getData() {
        return data;
    }
    public static void addNPC(NPC npc, BSShop shop){
        data.set(String.valueOf(npc.getId()), shop.getShopName());
        saveData();
    }
    public static void addPlayerNPC(NPC npc, UUID playerShopOwner){
        data.set(String.valueOf(npc.getId()),playerShopOwner.toString());
        saveData();
    }
    public static void removeNPC(NPC npc){
        data.set(String.valueOf(npc.getId()), null);
    }
    public static boolean isShopNPC(NPC npc){
        boolean b = true;
        if(!data.isSet(String.valueOf(npc.getId())))return false;
        BSShop shop = bossShop.getShop(data.getString(String.valueOf(npc.getId())));
        UUID uuid = null;
        try{
            uuid = UUID.fromString(data.getString(String.valueOf(npc.getId())));
        }catch (IllegalArgumentException e){
            //
        }
        if(uuid ==null&&shop==null)b =false;

        return b;
    }
    public static BSShop getShop(NPC npc){
        if(!isShopNPC(npc))return null;

        BSShop shop = bossShop.getShop(data.getString(String.valueOf(npc.getId())));
        if(shop==null){
            //shop = plugin.getPlayerShops().getShopsManager().getPlayerShop(UUID.fromString(data.getString(String.valueOf(npc.getId())))).getShop();
        }
        return shop;
    }
}
