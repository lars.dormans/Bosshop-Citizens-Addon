package me.creepercow.shopaddon.Tasks;

import me.creepercow.shopaddon.Commands.BindCommand;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class PlayerTimeOutTask implements Runnable{
    Player p;
    public PlayerTimeOutTask(Player p) {
        this.p = p;
    }

    @Override
    public void run() {
        if(!(BindCommand.playerShopMap.containsKey(p))&&!(BindCommand.removeRequest.contains(p))){
            return;
        }
        BindCommand.removeRequest.remove(p);
        BindCommand.playerShopMap.remove(p);
        p.sendMessage(ChatColor.RED+"Command timed out rerun the command please");
    }
}
