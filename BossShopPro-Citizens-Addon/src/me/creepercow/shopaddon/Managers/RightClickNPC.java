package me.creepercow.shopaddon.Managers;

import me.creepercow.shopaddon.Commands.BindCommand;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.black_ixx.bossshop.core.BSShop;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class RightClickNPC implements Listener{

    @EventHandler
    public void RightClick(NPCRightClickEvent e) {
        if (BindCommand.requestMap.containsKey(e.getClicker())||BindCommand.removeRequest.contains(e.getClicker())||BindCommand.playerShopMap.containsKey(e.getClicker())) {
            if(BindCommand.removeRequest.contains(e.getClicker())){
                BindCommand.RemoveContinue(e.getNPC(), e.getClicker());
                return;
            }
            if(BindCommand.playerShopMap.containsKey(e.getClicker())){
                BindCommand.PlayerCommandContinue(e.getNPC(),e.getClicker(),BindCommand.playerShopMap.get(e.getClicker()));
                return;
            }
            BSShop shop = BindCommand.requestMap.get(e.getClicker());
            BindCommand.CommandContinue(e.getNPC(), e.getClicker(), shop);
        }
        if (DataManager.isShopNPC(e.getNPC())) {
            BSShop shop = DataManager.getShop(e.getNPC());
            if (shop != null) {
                shop.openInventory(e.getClicker());
            }
        }

    }
}
